var page = 1;
var pages = 1;
var re = /[a-zA-z]{13,}/;
var users = [];

$(function () {
    while (page <= pages) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            async: false,
            url: 'https://reqres.in/api/users?page=' + page
        })
            .done(function (res) {
                pages = res.total_pages;
                if (page <= pages) {
                    res.data.forEach(user => {
                        users.push(user);
                    });
                    page++;
                } else {
                    pages = 0;
                }
            })
            .fail(function () {
                alert("Disculpe, estamos presentando problemas de conexión. Por favor intente más tarde ");
                pages = 0;
            })
    }
    compareRegex();
})

function compareRegex(option) {
    var matchCount = 0;
    var matchPer = 0;
    $("ul.user-list").html("");
    switch (option) {
        case 1:
            users.forEach(user => {
                if (re.test(user.first_name) && re.test(user.last_name)) {
                    addUser(user);
                    matchCount++;
                }
            });

            matchPer = matchCount / users.length * 100;
            break;

        default:
            users.forEach(user => {
                var name = user.first_name + user.last_name;
                console.log(name);
                if (re.test(name)) {
                    addUser(user);
                    matchCount++;
                }
            });

            matchPer = matchCount / users.length * 100;
            break;
    }

    $(".resume").html(matchPer.toFixed(2) + "% de coincidencias");

    var ctx = document.getElementById('myChart');
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: [matchPer.toFixed(2), (100 - matchPer).toFixed(2)],
                backgroundColor: ['rgba(120, 197, 255, 0.38)', 'rgba(188, 40, 255, 0.37)'],
                borderColor: ['#78c5ff', '#bc28ff']
            }],
            labels: [
                'coincidencias',
                'no conciden'
            ]
        }
    });
}

function addUser(user) {
    var item = '<li class="media p-2" id="' + user.id + '"><img src="' + user.avatar +
        '" class="mr-3 rounded-circle" alt="profile image"><div class="media-body"><h6 class="mt-0 mb-1">' + user.last_name +
        ' ' + user.first_name + '</h6><span>' + user.email + '</span></div></li>';

    $("ul.user-list").append(item);

}